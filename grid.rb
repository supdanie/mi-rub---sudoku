# frozen_string_literal: true

require 'cell'

# Contains sudoku game board
class Grid
  # Create Sudoku game grid of given dimension
  def initialize(dimension)
    @grid_size = dimension
    @grid_array = []
    (1..dimension).each do |x|
      row = []
      (1..dimension).each do |y|
        row.push(Cell.new(0, dimension))
      end
      @grid_array.push(row)
    end
  end

  # Return string with game board in a console friendly format
  def to_s(width = 3)
    block_size = Math.sqrt(@grid_size).to_i
    block_part = '-' * (block_size * width)
    block_part_with_plus = "#{block_part}+"
    line = block_part_with_plus * (block_size - 1) + block_part
    board = "#{line}"
    (1..@grid_size).each do |x|
      actual_line = ''
      (1..@grid_size).each do |y|
        value = @grid_array[x - 1][y - 1].value
        value_to_str = value.positive? ? value.to_s : ' '
        spaces_cnt = (width - value_to_str.length) / 2
        spaces_rest = width - value_to_str.length - spaces_cnt
        value_str = ' ' * spaces_cnt + value_to_str + ' ' * spaces_rest
        actual_line = "#{actual_line}#{value_str}"
        actual_line = "#{actual_line}|" if (y % block_size).zero? && y < @grid_size - 1
      end
      board = "#{board}\n#{actual_line}"
      board = "#{board}\n#{line}" if (x % block_size).zero?
    end
    board
  end

  # First element in the sudoku grid
  def first
    @grid_array[0][0]
  end

  # Last element in the sudoku grid
  def last
    last_index = @grid_size - 1
    @grid_array[last_index][last_index]
  end

  # Return value at given position
  def value(x, y) # rubocop:disable Naming/UncommunicativeMethodParamName
    @grid_array[x][y].value
  end

  # Marks number +z+ which shouldn't be at position [x, y]
  def exclude(x, y, z) # rubocop:disable Naming/UncommunicativeMethodParamNames
    @grid_array[x][y].exclude(z)
  end

  # True when there is already a number
  def filled?(x, y) # rubocop:disable Naming/UncommunicativeMethodParamName
    @grid_array[x][y].filled?
  end

  # True when no game was loaded
  def empty?
    filled?.zero?
  end

  # Yields elements in given row
  def row_elems(x) # rubocop:disable Naming/UncommunicativeMethodParamName
    @grid_array[x].each do |element|
      yield element if block_given?
    end
  end

  # Yields elements in given column
  def col_elems(y) # rubocop:disable Naming/UncommunicativeMethodParamName
    elements = []
    (1..@grid_size).each do |i|
      elements.push(@grid_array[i - 1][y])
    end

    elements.each do |element|
      yield element
    end
  end

  # Yields elements from block which is
  # containing element at given position
  def block_elems(x, y) # rubocop:disable Naming/UncommunicativeMethodParamName
    block_size = Math.sqrt(@grid_size).to_i
    block_number = x / block_size + y / block_size * block_size
    block(block_number).each do |element|
      yield element
    end
  end

  # With one argument return row, with 2, element
  # at given position
  def [](*args)
    row = args[0]
    if args.length == 1
      @grid_array[row]
    else
      col = args[1]
      @grid_array[row][col]
    end
  end

  # With one argument sets row, with 2 element
  def []=(*args)
    row = args[0]
    if args.length == 2
      value = args[1]
      @grid_array[row] = value
    else
      column = args[1]
      value = args[2]
      assign_value_and_exclude(row, column, value)
    end
  end

  def assign_value_and_exclude(x, y, value)
    @grid_array[x][y].value = value
    (1..@grid_size).each do |i|
      exclude(i - 1, y, value)
      exclude(x, i - 1, value)
    end
    sqrt = Math.sqrt(@grid_size).to_i
    start_x = x / sqrt * sqrt
    start_y = y / sqrt * sqrt
    end_x = start_x + sqrt - 1
    end_y = start_y + sqrt - 1
    (start_x..end_x).each do |index_x|
      (start_y..end_y).each do |index_y|
        exclude(index_x, index_y, value)
      end
    end
  end

  # Return number of missing numbers in grid
  def missing
    missing_cnt = 0
    each do |element|
      missing_cnt = element.value.zero? ? missing_cnt += 1 : missing_cnt
    end
    missing_cnt
  end

  # Number of filled cells
  def filled
    filled_cnt = 0
    each do |element|
      filled_cnt = element.filled? ? filled_cnt += 1 : filled_cnt
    end
    filled_cnt
  end

  # Number of rows in this sudoku
  def rows
    @grid_size
  end

  # Number of columns in this sudoku
  def cols
    @grid_size
  end

  # Iterates over all elements, left to right, top to bottom
  def each
    (1..@grid_size).each do |x|
      (1..@grid_size).each do |y|
        yield @grid_array[x - 1][y - 1] if block_given?
        return to_enum(:each) unless block_given?
      end
    end
  end

  def block(x)
    size = Math.sqrt(@grid_size).to_i
    start_row = x / size * size
    start_column = (x % size) * size
    end_row = start_row + size - 1
    end_column = start_column + size - 1
    block = []
    (start_row..end_row).each do |x|
      (start_column..end_column).each do |y|
        block.push(@grid_array[x][y])
      end
    end
    block
  end

  # Return true if no filled number break sudoku rules
  def valid?
    (0..(@grid_size - 1)).each do |index|
      row_elems(index).each do |cell|
        next if cell.value.zero?
        return false unless cell.possible.include? cell.value
      end

      col_elems(index) do |cell|
        next if cell.value.zero?
        return false unless cell.possible.include? cell.value
      end

      block(index).each do |cell|
        next if cell.value.zero?
        return false unless cell.possible.include? cell.value
      end
    end
    true
  end

  # Serialize grid values to a one line string
  def solution
    solution = ''
    each do |element|
      solution = "#{solution}#{element.value}"
    end
    solution
  end

  def map
    out = []
    if block_given?
      each { |element| out << yield(element) }
    else
      out = to_enum :map
    end
    out
  end

  def reduce(param, &block)
    result = param
    each { |element| result = block.call(result, element) }
    result
  end

  def dimension
    @grid_size
  end

  def unsolvable?
    each do |element|
      if element.value.zero? && element.possible.length.zero?
        return true
      end
    end
    false
  end
end
