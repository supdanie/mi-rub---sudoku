# frozen_string_literal: true

require 'grid'
# Parse string for 9x9 sudoku game
class StringParser
  # Static methods will follow
  class << self
    # Return true if passed object
    # is supported by this loader

    def supports?(arg)
      return false unless arg.class == String

      charset = ['.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
      arg.split('').each do |char|
        return false unless charset.include? char
      end
      arg.length.positive?
    end

    # Return Grid object when finished
    # parsing
    def load(arg)
      dimension = Math.sqrt(arg.length).to_i
      grid = Grid.new(dimension)
      charset = ['.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
      row = 0
      col = 0
      arg.split('').each do |char|
        return false unless charset.include? char

        grid[row, col] = char.to_i
        row += 1 if col == dimension - 1
        col = col == dimension - 1 ? 0 : col + 1
      end
      grid
    end
  end
end
