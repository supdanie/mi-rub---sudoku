# frozen_string_literal: true

require_relative './grid'
require_relative './string_parser'

# Basic sudoku solver
class Sudoku
  PARSERS = [StringParser].freeze

  EXCLUDE = proc do |enum, val|
    enum.each do |e|
      e.exclude(val)
    end
  end

  def initialize(game)
    @grid = load(game)
    @grid2 = load(game)
  end

  # Return true when there is no missing number
  def solved?
    !@grid.nil? && @grid.missing.zero?
  end

  def solve_sudoku(grid)
    (0..(grid.dimension - 1)).each do |x|
      found = false

      (0..(grid.dimension - 1)).each do |y|
        next if grid[x, y].value.positive?

        grid[x, y].possible.each do |value|
          found = true
          new_grid = load(grid.solution)
          new_grid[x, y] = value
          new_grid = fill_one_solution_fields(new_grid)
          next if new_grid.unsolvable?

          if new_grid.missing.zero?
            @grid = new_grid
            break
          end

          solve_sudoku(new_grid) if solved? == false
        end
        break
      end
      break if found
    end
  end

  # Solves sudoku and returns 2D Grid
  def solve
    raise 'invalid game given' unless @grid.valid?

    @grid = fill_one_solution_fields(@grid)
    solve_sudoku(load(@grid.solution)) if @grid.missing.positive?
    puts "missing values #{@grid.missing}, filled #{@grid.filled}"
    @grid
  end

  def fill_one_solution_fields(grid)
    count_ones_in_grid = 81
    while count_ones_in_grid.positive? && grid.unsolvable? == false do
      count_ones = 0
      (0..(grid.dimension - 1)).each do |x|
        (0..(grid.dimension - 1)).each do |y|
          next if grid[x, y].value.positive?

          if grid[x, y].possible.count == 1
            solution = grid[x, y].possible[0]
            grid[x, y] = solution
            count_ones += 1
          end
        end
      end
      count_ones_in_grid = count_ones
    end
    grid
  end

  def solution
    @grid.solution
  end

  protected

  def load(game)
    PARSERS.each do |p|
      return p.load(game) if p.supports?(game)
    end
    raise "input '#{game}' is not supported yet"
  end
end
